<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Github\Client;
use App\Util\Util;

class GithubController extends AbstractController
{    
    /**
     * @author Anthony BOUCHEREAU
     * count and list commits made for each calendar weeks
     * @param type $user github user
     * @param type $repository github repository
     * @param type $since date since (format yyyy-mm-dd), default 4 weeks ago
     * @param type $until date unitl (format yyyy-mm-dd), default today
     * @return JsonResponse
     */
    public function retrieveCommits($user, $repository, $since='default', $until='default') {
        
        //date parameters
        if ($since=='default') {
            $since = new \DateTime();
            $since->modify('-4 week');
        }
        else {
            $since = \DateTime::createFromFormat('Y-m-d', $since);
            if ($since===false) {
                throw new \Exception('Problem while parsing parameter Since');
            }
        }        
        
        if ($until=='default') {
            $until = new \DateTime();
        }
        else {
            $until = \DateTime::createFromFormat('Y-m-d', $until);
            if ($until===false) {
                throw new \Exception('Problem while parsing parameter Until');
            }
        }
        
        //Github API : search commits 
        $gitClient = new Client();
        $commits = $gitClient->api('repo')->commits()->all($user, $repository, array('sha' => 'master'));
        
        $commits = Util::filterCommits($commits,$since,$until);
        $commits = Util::groupByWeek($commits);
        return new JsonResponse($commits);
    }
   
}