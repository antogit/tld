<?php
namespace App\Util;

class Util
{
    /**
     * @author Anthony BOUCHEREAU
     * @param type $commits
     * @param type $since
     * @param type $until
     */
    static public function filterCommits($commits, $since, $until) {
        $filtered = [];
        foreach($commits as $commit) {
            //retrieve commit date
            $commitDate = $commit['commit']['author']['date'];
            $commitDate = \DateTime::createFromFormat('Y-m-d', substr($commitDate,0,10));  
            if ($commitDate >= $since && $commitDate <= $until) {
                $filtered[] = $commit;
            }
        }
        return $filtered;
    }
    
    /**
     * @author Anthony BOUCHEREAU
     * @param type $commits
     * @return type
     */
    
    static public function groupByWeek($commits) {
        $group = [];
        foreach($commits as $commit) {
            //retrieve commit date
            $commitDate = $commit['commit']['author']['date'];
            $commitDate = \DateTime::createFromFormat('Y-m-d', substr($commitDate,0,10)); 
            $year = $commitDate->format('Y');
            $week = $commitDate->format('W');
            
            //if week entry does not exist yet, initialize it
            $key = $year.'-'.$week;
            if(!isset($group[$key])) {
                $group[$key] = [
                  'year' => intval($year),
                  'week' => intval($week),
                  'count'=> 0,
                  'commits'=>[]
                ];
            }
            
            //add commit and set count += 1
            $group[$key]['count']++;
            $group[$key]['commits'][] = $commit;
        }
        
        //remove keys
        $group = array_values($group);
        return $group;
    }

}